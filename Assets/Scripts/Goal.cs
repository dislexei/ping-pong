﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour {
    //serves to indicate which player this goal belongs to
    private int id;

    public delegate void GoalScored(int i);
    public static event GoalScored OnGoal;

    //void Start()
    //{
    //    manager = GameObject.FindGameObjectWithTag("Manager");
    //}

    public void SetId(int newId)
    {
        id = newId;
    }

    public int GetId()
    {
        return id;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ball") {
            if (OnGoal != null)
            {
                OnGoal(id);
            }
        }
      
        
    }

}
