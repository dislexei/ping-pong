﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class OfflineBall : MonoBehaviour
{
    [Header("Ball Settings")]
    public float speed;
    private Vector3 moveDirection;
    private float speedIncrement;
    private float maxSpeed;

    [Header("Randomization options")]
    public bool randomBall = false;
    public bool randomizeDirection = false;

    public float minStartSpeed = 2.5F;
    public float maxStartSpeed = 5.0F;

    public float minStartEndSpeedDifference = 1.0F;
    public float maxStartEndSpeedDifference = 2.0F;

    public float minSpeedIncrement = 0.3F;
    public float maxSpeedIncrement = 0.6F;

    private GameObject[] paddles;
    private float paddleSize1;
    private float paddleSize2;

    private void OnEnable(){
        if (randomBall){
            GenerateBallColor();
            speed = Random.Range(minStartSpeed, maxStartSpeed);
            speedIncrement = Random.Range(minSpeedIncrement, maxSpeedIncrement);
            maxSpeed = Random.Range(speed + minStartEndSpeedDifference, speed + maxStartEndSpeedDifference);
        }
        else{
            speed = 3.0F;
            speedIncrement = 0.5F;
            maxSpeed = 10F;
        }

        while (Mathf.Abs(moveDirection.y) < 0.4) moveDirection = GenerateDirection();
        paddles = getPaddles();
        //TODO: fix array out of bound
        paddleSize1 = getPaddleSize(paddles, 0);
        paddleSize2 = getPaddleSize(paddles, 1);
    }

    public void StartMoving(){
        gameObject.GetComponent<Rigidbody>().velocity = moveDirection * speed;
    }

    private Vector3 GenerateDirection() {
        if (randomizeDirection){
            Vector3 direction = new Vector3(Random.Range(-1.0F, 1.0F), Random.Range(-1.0F, 1.0F), 0);
            Vector3 normailized = direction.normalized;
            return normailized;
        }
        else return new Vector3(0, 1, 0);
    }

    private void GenerateBallColor(){
        Color newColor = Random.ColorHSV();
        SpriteRenderer ballSprite = gameObject.GetComponentInChildren<SpriteRenderer>();
        ballSprite.color = newColor;
    }

    private void IncreaseSpeed(float increment){
        if (speed + increment < maxSpeed){
            speed += increment;
        }
        else{
            speed = maxSpeed;
        }
    }

    public GameObject[] getPaddles(){
        return GameObject.FindGameObjectsWithTag("Paddle");
    }

    private float getPaddleSize(GameObject[] _paddles, int number){
        return _paddles[number].GetComponent<Collider>().bounds.size.x;
    }


    private Vector3 BouncePaddle(float paddleXPosition, float ballXposition){
        //determines where exactly the ball hits on the paddle
        float positionDifference = ballXposition - paddleXPosition;
        float positionPercentage = positionDifference / paddleSize1 * speed;
        moveDirection = gameObject.GetComponent<Rigidbody>().velocity;
        Vector3 newDirection = new Vector3(moveDirection.x + positionPercentage * 1.8F, moveDirection.y * -1, 0);

        Vector3 normalized = newDirection.normalized;
        IncreaseSpeed(speedIncrement);
        return normalized;
    }

    private void OnTriggerEnter(Collider collision){
        if (collision.gameObject.tag == "Vertical"){
            IncreaseSpeed(speedIncrement);
            gameObject.GetComponent<Rigidbody>().velocity = new Vector3(gameObject.GetComponent<Rigidbody>().velocity.x * -1, gameObject.GetComponent<Rigidbody>().velocity.y, 0).normalized * speed;
        }

        if (collision.gameObject.tag == "Paddle"){
           moveDirection = BouncePaddle(collision.gameObject.transform.position.x, this.gameObject.transform.position.x);
           gameObject.GetComponent<Rigidbody>().velocity = moveDirection * speed;
        }
    }
   
}
