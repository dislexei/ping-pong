﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class OfflinePlayer : MonoBehaviour {

    public float speed;
    private bool moveableRight = true;
    private bool moveableLeft = true;

    // Use this for initialization
    void Start () {
        speed = 5; 
	}
	

	void FixedUpdate () {
        if (Input.GetAxis("Horizontal") != 0){
            MovePaddle(Input.GetAxis("Horizontal"));
        }
        else StopPaddle();
    }

    private void OnTriggerEnter(Collider collision){
        if (collision.gameObject.tag == "Vertical"){
            if (Input.GetAxis("Horizontal") > 0){
                moveableRight = false;
            }
            else moveableLeft = false;
        }
    }

    private void OnTriggerExit(Collider collision){
        if (collision.gameObject.tag == "Vertical"){
            moveableRight = true;
            moveableLeft = true;
        }
    }


    void MovePaddle(float direction){
        //check right wall
        if (direction > 0){
            if (moveableRight){
                gameObject.GetComponent<Rigidbody>().velocity = new Vector2(direction * speed, 0F);
            }
            else StopPaddle();
        }

        //check left wall
        if (direction < 0){
            if (moveableLeft) {
                gameObject.GetComponent<Rigidbody>().velocity = new Vector2(direction * speed, 0F);
            }
            else StopPaddle();
        }
    }

    void StopPaddle(){
        gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
    
}
