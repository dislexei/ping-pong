﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Ball : NetworkMessageHandler
{
    [Header("Ball Settings")]
    public float speed;
    public Vector3 moveDirection;
    public bool randomBall = true;
    public bool randomizeDirection = true;

    [Header("Network Messaging Settings")]
    public bool canSendNetworkMovement;
    public float netSpeed;
    public int networkSendRate = 5;
    public float timeBetweenMovementStart;
    public float timeBetweenMovementEnd;

    [Header("Lerping Properties")]
    public bool isLerpingPosition;
    public Vector3 realPosition;
    public Vector3 lastRealPosition;
    public float timeStartedLerping;
    public float timeToLerp;

    private float speedIncrement;
    private float maxSpeed;

    private GameObject[] paddles;
    private float paddleSize1;
    private float paddleSize2;

    private void OnEnable()
    {
        if (randomBall)
        {
            //gameObject.GetComponent<Renderer>().material.color = Random.ColorHSV();
            GenerateBallColor();
            speed = Random.Range(5.0F, 1.0F);
            speedIncrement = Random.Range(0.5F, 2F);
            maxSpeed = Random.Range(speed, 20F);
        }
        else
        {
            speed = 3.0F;
            speedIncrement = 0.5F;
            maxSpeed = 10F;
        }

        while (Mathf.Abs(moveDirection.y) < 0.4) moveDirection = GenerateDirection();
        paddles = getPaddles();
        //TODO: fix array out of bound
        paddleSize1 = getPaddleSize(paddles, 0);
        //paddleSize2 = getPaddleSize(paddles, 1);
        gameObject.GetComponent<Rigidbody>().velocity = moveDirection * speed;
    }

    private Vector3 GenerateDirection() {
        if (randomizeDirection){
            Vector3 direction = new Vector3(Random.Range(-1.0F, 1.0F), Random.Range(-1.0F, 1.0F), 0);
            Vector3 normailized = direction.normalized;
            return normailized;
        }
        else return new Vector3(0, 1, 0);
    }

    private void GenerateBallColor()
    {
        SpriteRenderer ballSprite = gameObject.GetComponentInChildren<SpriteRenderer>();
        ballSprite.color = Random.ColorHSV();
    }

    private void IncreaseSpeed(float increment)
    {
        if (speed + increment < maxSpeed)
        {
            speed += increment;
        }
        else
        {
            speed = maxSpeed;
        }
    }

    private GameObject[] getPaddles()
    {
        return GameObject.FindGameObjectsWithTag("Paddle");
    }

    private float getPaddleSize(GameObject[] _paddles, int number)
    {

        return _paddles[number].GetComponent<Collider>().bounds.size.x;

    }

    private Vector3 BouncePaddle(float paddleXPosition, float ballXposition)
    {
        //determines where exactly the ball hits on the paddle
        float positionDifference = ballXposition - paddleXPosition;
        float positionPercentage = positionDifference / paddleSize1 * speed;
        moveDirection = gameObject.GetComponent<Rigidbody>().velocity;
        Vector3 newDirection = new Vector3(moveDirection.x + positionPercentage * 1.8F, moveDirection.y * -1, 0);

        Vector3 normalized = newDirection.normalized;
        IncreaseSpeed(speedIncrement);
        return normalized;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Vertical")
        {
            IncreaseSpeed(speedIncrement);
            gameObject.GetComponent<Rigidbody>().velocity = new Vector3(gameObject.GetComponent<Rigidbody>().velocity.x * -1, gameObject.GetComponent<Rigidbody>().velocity.y, 0).normalized * speed;
        }

        if (collision.gameObject.tag == "Paddle")
        {
            moveDirection = BouncePaddle(collision.gameObject.transform.position.x, this.gameObject.transform.position.x);
            gameObject.GetComponent<Rigidbody>().velocity = moveDirection * speed;
        }
    }

    // NETWORKED MOVEMENT BELOW

    private void Start()
    {
        if (!isServer){
            Debug.Log("This is a CLIENT instance of the game.");
            canSendNetworkMovement = false;
            RegisterNetworkMessages();
        }
        else {
            Debug.Log("This is a HOST instance of the game.");
            isLerpingPosition = false;
            realPosition = transform.position;
        }
        
    }

    private void RegisterNetworkMessages(){
        NetworkManager.singleton.client.RegisterHandler(movement_msg, OnReceiveMovementMessage);
    }

    private void OnReceiveMovementMessage(NetworkMessage message)
    {
        BallMovementMessage _message = message.ReadMessage<BallMovementMessage>();
        //set the local ball position here

        if (!isServer)
        {
            lastRealPosition = realPosition;
            realPosition = _message.ballPosition;
            //ensures correct timeframe between messages even though it's set to 1/5 seconds through courutine
            timeToLerp = _message.time;

            if (realPosition != transform.position) {
                isLerpingPosition = true;
            }
            timeStartedLerping = Time.time;
        }
    }

    private void FixedUpdate() {
        if (!canSendNetworkMovement)
        {
            canSendNetworkMovement = true;
            StartCoroutine(StartNetworkSendCooldown());
        }

        if (!isServer)
        {
            NetworkLerp();
        }
    }

    private void NetworkLerp()
    {
        if (isLerpingPosition)
        {
            //gets a percentage for the lerp that is supposed to go by
            float lerpPercentage = (Time.time - timeStartedLerping) / timeToLerp;
            transform.position = Vector3.Lerp(lastRealPosition, realPosition, lerpPercentage);
        }
    }

    private IEnumerator StartNetworkSendCooldown() {
        timeBetweenMovementStart = Time.time;
        yield return new WaitForSeconds(1 / networkSendRate);
        SendNetworkMovement();
    }

    private void SendNetworkMovement() {
        timeBetweenMovementEnd = Time.time;
        SendMovementMessage(transform.position, (timeBetweenMovementEnd - timeBetweenMovementStart));
        canSendNetworkMovement = false;
    }

    private void SendMovementMessage(Vector3 position, float timeToLerp)
    {
        BallMovementMessage msg = new BallMovementMessage() {
            ballPosition = position,
            time = timeToLerp
        };
        if (isServer)
        {
            NetworkServer.SendToAll(movement_msg, msg);
        }
    }


    
}
