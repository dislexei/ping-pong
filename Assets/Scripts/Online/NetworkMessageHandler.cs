﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

//cannot be instantiated on its own
public abstract class NetworkMessageHandler : NetworkBehaviour {


    //set network message identity
    public const short movement_msg = 998;
    public const short score_msg = 999;

    // Update is called once per frame
    public class BallMovementMessage : MessageBase{
        public Vector3 ballPosition;
        public float time;
    }

    public class ScoreMessage : MessageBase {
        public int[] score;
    }
}
